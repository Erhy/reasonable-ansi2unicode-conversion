﻿# purpose is to detect text files coded in ANSI with e.g. Ä ä Ö ö Ü ü ß § ² ³ µ

# no  warranty, use at your own risk

# examples for $rootOfDirectoryTreeToRun
# $rootOfDirectoryTreeToRun = 'C:\'
# $rootOfDirectoryTreeToRun = 'C:\Users\gle\AppData' # necessary because Folder Appdata has the Hide attribute
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# please specify your preferred directory and file for list locations

$rootOfDirectoryTreeToRun = 'H:\Users\gle\Documents\Prog'

$fileToListFilesForConversion = 'H:\Users\gle\Documents\Prog\ListToConvert.txt'

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# please specify the maximum percent in the files of characters above 0x7f 
#   that the file is not classified as binary
#  with Cyrillic most characters are above 0x7f,  so 100 is the right setting

$prozentOfMaxCharsAbove7f = 10

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
<#

 this should be installed:

 PowerShell 7     see https://github.com/PowerShell/PowerShell/releases 
                   at 1st April 2020 the latest release is v7.0.0
                   download and install v7.0.0 https://github.com/PowerShell/PowerShell/releases/download/v7.0.0/PowerShell-7.0.0-win-x86.msi

 VSCode            see download page https://code.visualstudio.com/

 VIM 8.2 for conversion    see  https://www.vim.org/
                           download and install https://ftp.nluug.nl/pub/vim/pc/gvim82.exe

 run this script in Windows VSCode with powerShell Default Version    PowerShell Core 7 (x64)

 then the script convert_to_UTF8_with_BOM_from_list.ps1 performs the conversion
#>
try {
    New-Item $fileToListFilesForConversion -ItemType file -force | out-null
} catch { }

$fileToLog = ( $fileToListFilesForConversion + '.log' )
try {
    New-Item $fileToLog -ItemType file -force | out-null
} catch { }

if ( -not ( Test-Path -LiteralPath $fileToListFilesForConversion -PathType Leaf ) ) {
    Write-Host ( 'list file could not be created: ' + $fileToListFilesForConversion )
    exit 254
}

if ( -not ( Test-Path -LiteralPath $fileToLog -PathType Leaf ) ) {
    Write-Host ( 'log file could not be created: ' + $fileToLog )
    exit 253
}

Add-Content -LiteralPath $fileToLog ( 'generate file list for conversion to Unicode by parsing directory tree: ' + "`r`n" + $rootOfDirectoryTreeToRun + "`r`n" )
Add-Content -LiteralPath $fileToLog ( 'list of files for conversion:' + "`r`n" + $fileToListFilesForConversion + "`r`n" )

# to access HKEY_CLASSES_ROOT with HKCR
New-PSDrive -Name HKCR -PSProvider Registry -Root HKEY_CLASSES_ROOT -ErrorAction SilentlyContinue | out-null

# list of directories to parse
# $dirList = @( $rootOfDirectoryTreeToRun ) # the first entry in the list of 
$allFiles = @( ) # init array for all Files
# to identify restry entries for text files
$textIdent = 'text'
$textIdentL = $textIdent.Length

$withQuestionmark = 'Ä�' # to use second character in IndexOf 

# collections of extensions veryfied
$tRegNot =  @{}
$tRegYes =  @{}

$iCount = 0 #counter of parsed files

$foundCount = 0 #counter of found files to convert

$maxFileLenForConvert = ( 1000 * 1000 * 2 ) # 2 MegaByte for text files seems a good boundery

#buffer to read BOM bytes
$minLenOfFile = 32
$chunkForBOMandFileBeginLength = $minLenOfFile
$chunkForBOMandFileBegin = New-Object byte[] $chunkForBOMandFileBeginLength

# some extensions not to parse
$excludedUnsorted = @( '*_NotConverted.*', '*.db', '*.pdf', '*.xaml', '*.csproj', '*.sln', '*.doc', '*.docx', '*.pdb', '*.pch', '*.ipch', `
                '*.lnk', '*.vcproj', '*.vcproj~', '*.obj', '*.iobj', '*.exe', '*.com', '*.dll', '*.tmp', `
                '*.inf', '*.log', '*.pyproj', '*.vbproj', '*.vcxproj', '*.vcxproj~', '*.cache', `
                '*.filters', '*.htm' , '*.html', '*.mht', '*.tlh', '*.tli', '*.png', '*.jpg', '*.gif', `
                '*.ico', '*.bmp', '*.tif', '*.wav', '*.mp3', '*.url', '*.res', '*.zip', '*.avi', '*.au', `
                '*.bin', '*.cab', '*.bak', '*.chm', '*.cpl', '*.eml', '*.hlp', '*.img', '*.iso', '*.jar', `
                '*.json', '*.mid', '*.mp4', '*.ocx', '*.odt', '*.ogg', '*.ppt', '*.pptx', '*.wmv', '*.wri', `
                '*.xls', '*.xlsx', '*.x')
                
$excluded = $excludedUnsorted | Sort-Object 

Write-Host ('Begin -generate file list for conversion to Unicode- Begin')

# avoid that Windows sleep during run of the script
# like in script from Roger Qiu ,  see  https://gist.github.com/CMCDragonkai/bf8e8b7553c48e4f65124bc6f41769eb#file-stay-awake-ps1
$CodeToPublicSetThreadExe=@'
[DllImport("kernel32.dll", CharSet = CharSet.Auto,SetLastError = true)]
public static extern void SetThreadExecutionState(uint esFlags);
'@
$ste = Add-Type -memberDefinition $CodeToPublicSetThreadExe -name System -namespace Win32 -passThru
$ES_SYSTEM_REQUIRED = [uint32]"0x00000001"

Write-Host "Staying Awake"

# because the Get-ChildItem option -Recurse not works as suggested I code it

# parse folders ( = directories )
$additionalDirs = @( $rootOfDirectoryTreeToRun )
$dirListLength = 0
do {
    # search files step by step
    $ste::SetThreadExecutionState( $ES_SYSTEM_REQUIRED) # reset system activity timers 
    $dirListLengthNow = $additionalDirs.Length
    Write-Host ( 'number of further directories found : ' + $dirListLengthNow )
    $dirListLength += $dirListLengthNow

     # parse files 
    $allFiles += (Get-ChildItem -LiteralPath $additionalDirs -exclude $excluded `
        -Attributes !Hidden+!System+!ReadOnly+!IntegrityStream+!ReparsePoint+!Encrypted+!Directory `
        -ErrorAction SilentlyContinue).FullName # only FullName because sort
    
    $ste::SetThreadExecutionState( $ES_SYSTEM_REQUIRED) # reset system activity timers 
    
    $additionalDirs = `
    ( Get-ChildItem -LiteralPath $additionalDirs  `
        -Attributes Directory+!Hidden+!System+!IntegrityStream+!ReparsePoint+!Encrypted `
        -ErrorAction SilentlyContinue ).FullName

    $ste::SetThreadExecutionState( $ES_SYSTEM_REQUIRED) # reset system activity timers 

} while ( $null -ne $additionalDirs )

Write-Host ( 'number of all directories to parse : ' + $dirListLength )

$ste::SetThreadExecutionState( $ES_SYSTEM_REQUIRED) # reset system activity timers 

$allFilesSorted = $allFiles | Sort-Object

Write-Host ( 'file list sorted with number of files : ' + $allFilesSorted.Length )
$fileNow = $null

$allFilesSorted.ForEach( {
    
    if ( 0 -eq ($iCount % 100) ) {
        Write-Host ('progress - processed files: ' + $iCount)
        $ste::SetThreadExecutionState( $ES_SYSTEM_REQUIRED) # reset system activity timers 
    }
    $iCount += 1

    try {
        $fileNow = Get-Item -LiteralPath $PSItem
    } catch {
        $fileNow = $null
    }
    if ( $null -eq $fileNow ) {
        Add-Content -LiteralPath $fileToLog ( 'now file does not exist : ' + $PSItem )
        return
    }
    # Write-Host ('now for file : ' + $fileNow.FullName )

    # test file extensions registered in Windows registry

    $eXt = $fileNow.Extension
    
    if ( $eXt.Length -le 1 ) { return } # skip, this extension is too short to be verified

    if ( $tRegNot.ContainsKey($eXt) ) { return } # collected at non text type

    if ( -not $tRegYes.ContainsKey($eXt) ) { # not collected as valid text type

        $regPathToAccess = ( 'HKCR:\' + $eXt )
        $pathExists = Test-Path -path $regPathToAccess
        if ( -not $pathExists )  {
            $tRegNot[$eXt] = $null ;        return # file extension not registered
        }

        $regPathList = $null
        $regPathList = Get-ItemProperty -path $regPathToAccess -ErrorAction ignore
        if ( $null -eq $regPathList ) { 
            $tRegNot[$eXt] = $null ;        return  # skip, because registry path has no property
        }

        # access properties 'PerceivedType' and 'Content Type' and '(default)'
        $contFromDefaultString = $null
        $contFromDefaultString = $regPathList.'(default)'
        if ( $null  -ne $contFromDefaultString ) { # default registry property found
            if ( $null -ne ($contFromDefaultString | Select-String -Pattern 'GOOGLE' -SimpleMatch ) ) {
                Add-Content -LiteralPath $fileToLog ( 'Google: ' + $fileNow.FullName )
                $tRegNot[$eXt] = $null ; return # google files not necessary to convert
            }
        }
        
        # test 'PerceivedType' and 'Content Type' properties for $textIdent
        $testTextInPropsResult = $false

        $propPerhapsWithText = $null
        $propPerhapsWithText = $regPathList.'PerceivedType'
        if (  $null -eq $propPerhapsWithText ) {
            $propPerhapsWithText = $regPathList.'Content Type'
        }
        if ( $null -ne $propPerhapsWithText ) {
            if ( $propPerhapsWithText.Length -ge $textIdentL ) {
                if ( $textIdent -eq $propPerhapsWithText.SubString(0, $textIdentL) ) {
                    $testTextInPropsResult = $true
                }
            }
        }

        if ( -not $testTextInPropsResult ) { # no success to find 'text' in properties
            # 'VBSFile' is a special case
            if  (   $null -eq ( $contFromDefaultString | Select-String -Pattern 'VBSFile' -SimpleMatch ) -and `
                    $null -eq ( $contFromDefaultString | Select-String -Pattern 'script' -SimpleMatch ) -and `
                    $null -eq ( $contFromDefaultString | Select-String -Pattern 'batfile' -SimpleMatch ) -and `
                    $null -eq ( $contFromDefaultString | Select-String -Pattern 'cmdfile' -SimpleMatch ) -and `
                    $null -eq ( $contFromDefaultString | Select-String -Pattern 'JSFile' -SimpleMatch ) ) {

                        $tRegNot[$eXt] = $null ; return # no success again
                }
        }

        $tRegYes[$eXt] = 0 # register as valid text type 

    }

    # check BOM
    try {
        $fileStream = New-Object System.IO.FileStream $fileNow.FullName, ([IO.FileMode]::Open), ([IO.FileAccess]::ReadWrite), ([IO.FileShare]::ReadWrite)
    }
    catch {
        $fileStream = $null
    }

    $writeable = $true
    if ( $null -eq $fileStream )  {
        $writeable = $false
    }
    elseif ( -not $fileStream.CanWrite ) {
        $fileStream.Close(); $fileStream = $null ;  $writeable = $false
    } 
    if ( -not $writeable ) { # only interested on writable files
        Add-Content -LiteralPath $fileToLog ( 'not writeable : '  + $fileNow.FullName )
        return
    }

    $FileLen = $fileStream.length

    if ( $FileLen -lt $minLenOfFile ) { # not interested on very short files
        if ( $FileLen -gt 0 ) {
            Add-Content -LiteralPath $fileToLog ( 'too short: '  + $fileNow.FullName )
        }
        $fileStream.Close(); $fileStream = $null ; return
    } 
    if ( $FileLen -gt $maxFileLenForConvert ) { # not interested on very long files
        Add-Content -LiteralPath $fileToLog ( 'too long: '  + $fileNow.FullName )
        $fileStream.Close(); $fileStream = $null ; return
    } 

    $chunkForBOMReaded = $fileStream.Read($chunkForBOMandFileBegin, 0, $chunkForBOMandFileBeginLength )
    $fileStream.Close()
    $fileStream = $null # close

    if ( $chunkForBOMReaded -lt $chunkForBOMandFileBeginLength ) { return } # rare failure

    # following like https://stackoverflow.com/questions/54534765/convert-sources-to-utf-8-without-bom
    # convert to HEX string
    $allFirstHex  = ''
    For ( $i=0 ; $i -lt $chunkForBOMandFileBeginLength ; $i++ ) {
        $allFirstHex  += '{0:x2}' -f $chunkForBOMandFileBegin[$i]
    }
    
    $first4Hex = $allFirstHex.Substring(0,8)
    $first3Hex = $allFirstHex.Substring(0,6)
    $first2Hex = $allFirstHex.Substring(0,4)
    
    $BOMkind = ''
    if     ( $first4Hex -eq '0000FEFF' ) { $BOMkind = 'UTF-32BE BOM' } # UTF-32BE BOM
    elseif ( $first4Hex -eq 'FFFE0000' ) { $BOMkind = 'UTF-32LE BOM' } # UTF-32LE BOM 
    elseif ( $first4Hex -eq 'DD736673' ) { $BOMkind = 'UTF-EBCDIC BOM' } # UTF-EBCDIC BOM
    elseif ( $first4Hex -eq '84319533' ) { $BOMkind = 'GB 18030 BOM' } # GB 18030 BOM
    elseif ( $first3Hex -eq 'EFBBBF' )   { $BOMkind = 'UTF-8 BOM' } # UTF-8 BOM
    elseif ( $first2Hex -eq 'FFFE' )     { $BOMkind = 'UTF-16LE BOM' } # UTF-16LE BOM
    elseif ( $first2Hex -eq 'FEFF' )     { $BOMkind = 'UTF-16BE BOM' } # UTF-16BE BOM

    if ( [bool]$BOMkind ) {
        # Add-Content -LiteralPath $fileToLog ( 'BOM detected : '  + $fileNow.FullName )
        return # BOM detected, don't need to check for ANSI
    }
    if ( $chunkForBOMandFileBegin[0] -eq 0 ) {
        Add-Content -LiteralPath $fileToLog ( 'binary zero on start : '  + $fileNow.FullName )
        return
    }
    if ( $null -ne ( $allFirstHex | Select-String -Pattern '0000' -SimpleMatch ) ) {
        Add-Content -LiteralPath $fileToLog ( 'binary zeros on start : '  + $fileNow.FullName )
        return
    }

    try {
        $contentUTF = Get-Content -LiteralPath $fileNow.FullName -Encoding UTF8NoBOM -Raw # read file in one string
    } catch {
        $contentUTF = $null
    }
    if (  $null -eq $contentUTF ) {
        Add-Content -LiteralPath $fileToLog ( 'Get-Content fails for ' + $fileNow.FullName )
        return
    }
    
    if ( $prozentOfMaxCharsAbove7f -lt 100 ) {
        $countOfQuMaxAccepted = ( $FileLen * $prozentOfMaxCharsAbove7f ) / 100
    } else {
        $countOfQuMaxAccepted = 1 # to limit the search for '�'
    }
    
    $iXrunning = 0
    $ixCount = 0
    while( $iXrunning -ge 0  -and $ixCount -le $countOfQuMaxAccepted ) {
        # tricky, because IndexOf don't work with '�' Parameter
        $iXrunning = $contentUTF.indexOf( $withQuestionmark[1], $iXrunning);
        if ( $iXrunning -ge 0 ) {
            $ixCount += 1
            $iXrunning += 1
        }
    }
    if ( $ixCount -eq 0 ) {
        return #no unknown charaters
    }
    if ( $ixCount -gt $countOfQuMaxAccepted -and $prozentOfMaxCharsAbove7f -lt 100 ) {
        Add-Content -LiteralPath $fileToLog ( 'binary: ' + $fileNow.FullName )
        return #too much unknown charaters
    }
    $foundCount += 1
    Add-Content -LiteralPath $fileToListFilesForConversion $fileNow.FullName
    $tRegYes[$eXt] += 1 # for this extension number of files where conversion is necessary
})

$fileNow = $null # to close the last used file item

Add-Content -LiteralPath $fileToLog ( "`r`nfile extensions excluded: `r`n" + $excluded )
Add-Content -LiteralPath $fileToLog ( "`r`nfile extensions skipped: `r`n" + ( $tRegNot.Keys | Sort-Object ) )
Add-Content -LiteralPath $fileToLog ( "file extensions observed: `r`n" + ( $tRegYes.Keys | Sort-Object ) )
Add-Content -LiteralPath $fileToLog ( "`r`nnumber of files parsed: " + $iCount )
Add-Content -LiteralPath $fileToLog ( 'number of files found which should be converted: ' + $foundCount + "`r`n" )
$tRegYes.Keys | ForEach-Object {
    $countToConvertForThisExtension = $tRegYes[$_]
    if ( $countToConvertForThisExtension -gt 0 ) {
        Add-Content -LiteralPath $fileToLog ( 'for ' + $_ + ' : ' + $countToConvertForThisExtension )
    }
}
Write-Host ('see Log: ' + $fileToLog )
Write-Host ('files to convert listed in: ' + $fileToListFilesForConversion)
Write-Host ('DONE -generate file list for conversion to Unicode- DONE')
